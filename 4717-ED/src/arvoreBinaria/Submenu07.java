/*
 * 09/05/2016
 * @author 142016-1 Fernando Moraes Oliveira
 */

package arvoreBinaria;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Submenu07 {

	static Arvore arvore = new Arvore();

	public static void main(String args[]) {

		String opt;
		int t;
		
		do {
			opt = (String) JOptionPane.showInputDialog( null,
					"Escolha uma Opção:\n\n"
							+ "01 - Adciona Elemento\n"
							+ "02 - Verifica Elemento\n"
							+ "00 - FIM\n\n"
							,"Menu Árvore Binária", JOptionPane.QUESTION_MESSAGE, 
							new ImageIcon("../4717-ED/icones/bot.png"), null, null);

			String msg = "";

			switch (opt) {

			case "1": //Adiciona Elemento
				t = Integer.parseInt(JOptionPane.showInputDialog(null, "Digite a quantidade de elementos: ",
						"Elementos da Árvore", JOptionPane.QUESTION_MESSAGE));
				for ( int i = 0; i < t; i++){
					int e = Integer.parseInt(JOptionPane.showInputDialog(null, "Digite um número inteiro: ",
							"Elemento da Árvore", JOptionPane.QUESTION_MESSAGE));
					arvore.adicionaElemento(e);
				}
				JOptionPane.showMessageDialog(null, "Elementos: " + msg, "Árvore elementos…", JOptionPane.INFORMATION_MESSAGE);
				break;

			case "2": //Verifica Elemento
				int e = Integer.parseInt(JOptionPane.showInputDialog(null, "Digite um número inteiro: ",
						"Elemento da Árvore", JOptionPane.QUESTION_MESSAGE));
				arvore.verifica(e);
				break;
				
			case "0":
				return;
				
			default://Tratamento de Erro
				JOptionPane.showMessageDialog(null, "Por favor, escolha as Opções do Menu!");
				break;
			}
		} while (opt != "0");
	}
}