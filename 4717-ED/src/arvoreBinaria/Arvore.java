package arvoreBinaria;

public class Arvore {
	
	private No raiz;
	
	public Arvore ( ){
		raiz = null;
	}
	
	
	public void adicionaElemento(int e){

		No novo = new No(e);

		if (raiz == null)
			raiz = novo;
		else{
			No aux1 = raiz, aux2 = raiz;
			while ( aux1 != null ){
				aux2=aux1;
				if ( e < aux1.elemento )
					aux1 = aux1.esquerda;
				else if ( e > aux1.elemento )
					aux1 = aux1.direita;
			}
			if (e == aux2.elemento)
				System.out.println( "Elemento já existe" );
			else{
				if ( e < aux2.elemento ) aux2.esquerda = novo;
				if ( e > aux2.elemento ) aux2.direita = novo;
				System.out.println( "Elemento Incluído" );
			}
		}
	}
	
	
	/**
	Informa se o elemento já existe na árvore. @param e elemento para buscar
	 */
	public void verifica(int e){
		
		No aux = raiz;
		
		while (aux != null){

			if (e < aux.elemento) aux = aux.esquerda;
			else if (e > aux.elemento)
				aux = aux.direita;
			else
				System.out.println( "Elemento Repetido");
		}
	}
	
	
	/**Exibir os elementos em Pós-Ordem*/
	public void posOrdem (No n) {
		
		if ( n != null ){
			posOrdem ( n.esquerda );
			posOrdem ( n.direita );
				System.out.println( n.elemento );
		}
	}
	
	
	/**Percorre os elementos somando em Pós-Ordem*/
	public int somaPosOrdem ( No n) {
		
		if ( n != null ){
			return somaPosOrdem( n.esquerda )
					+ somaPosOrdem ( n.direita )
					+ n.elemento;
		}
		return 0;
	}
}