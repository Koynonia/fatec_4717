/**
 *
 * @author tecnico
 */
package recursividade;

public class Numero {
    
    public static void main(String args[]){
        Numero n = new Numero();
        double s=n.serie(0,1.0,10);
        System.out.println("Res: " + s);
        double x=n.serie2(10);
        System.out.println("Res: " + x);
    }
    
    public double serie (int i, double j, int m){
        double k=1;
        if (i==0)
            return 1 + serie (i+1, 1, m);
        if (i <= m){ 
            j = j * i;
            return 1/j + serie (i+1, j, m);
         }
        return 0;
    }
    
    public int fatorial (int N){
        if (N== 0 || N==1)
            return 1;
        return N*fatorial(N-1);
    }
    
    public double serie2 (int N){
        if (N >= 2){
            return 1.0/fatorial(N) + serie2(N-1);
        }
        if (N == 1)
            return 1.0 + serie2(N-1);
        return 1.0;
    }
}
