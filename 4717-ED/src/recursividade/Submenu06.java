/*
 * 09/05/2016
 * @author 142016-1 Fernando Moraes Oliveira
 */

package recursividade;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Submenu06 {
	
	static CalculaFatorial fatorial = new CalculaFatorial();
	static CalculaSoma soma = new CalculaSoma();
	static Ordena ordena = new Ordena();
	
    public static void main(String args[]) {

        String opt;
        int n;
        
        do {
            opt = (String) JOptionPane.showInputDialog( null,
                    "Escolha uma Opção:\n\n"
                    + "01 - Calcula Fatorial\n"
                    + "02 - Calcula Fatorial (Recursiva com Continuidade)\n"
                    + "03 - Calcula Fatorial (Recursiva com Parada)\n"
                    + "04 - Calcula Soma\n"
                    + "05 - Calcula Soma (Recursividade)\n"
                    + "06 - Merge Sort (Ordena Crescente)\n"
                    + "07 - Merge Sort (Ordena Decrescente)\n"
                    + "08 - Quick Sort (Ordena Crescente)\n"
                    + "09 - Quick Sort (Ordena Decrescente)\n"
                    + "00 - FIM\n\n"
                    ,"Menu Recursividade", JOptionPane.QUESTION_MESSAGE, 
                    new ImageIcon("../4717-ED/icones/bot.png"), null, null);
            
            String msg = "";
            
            switch (opt) {
                case "1": //FATORIAL
                    n = Integer.parseInt(JOptionPane.showInputDialog("Digite um Número Inteiro: "));
                    fatorial.fatorial ( n );
                    break;
                    
                case "2": //FATORIAL Recursivo Continuidade
                    n = Integer.parseInt(JOptionPane.showInputDialog("Digite um Número Inteiro: "));
                    fatorial.fatC ( n );
                    break;
                    
                case "3"://FATORIAL Recursivo Parada
                    n = Integer.parseInt(JOptionPane.showInputDialog("Digite um Número Inteiro: "));
                    fatorial.fatP( n );
                    break;
                    
                case "4": //Soma
                	n = Integer.parseInt(JOptionPane.showInputDialog("Digite um Número Inteiro: "));
                	soma.soma(n);
                    break;
                    
                case "5": //Calcula Recursivo
                	n = Integer.parseInt(JOptionPane.showInputDialog("Digite um Número Inteiro: "));
                	int m = Integer.parseInt(JOptionPane.showInputDialog("Digite um Número Inteiro: "));
                	System.out.println( soma.somaRecursiva(n, m) );
                    break;
                    
                case "6": //Ordena Crescente Merge Sort
                	int tam_mc = Integer.parseInt(JOptionPane.showInputDialog(null, "Digite o tamanho do Vetor: ",
                			"Merge Sort", JOptionPane.QUESTION_MESSAGE));
                	int vet_mc[] = new int[tam_mc];
                	for (  int i = 0;  i < vet_mc.length; i++ ){
                	int e = Integer.parseInt(JOptionPane.showInputDialog(null, "Digite o elemento do Índice [" + i + "]: ",
                			"Elementos do Vetor", JOptionPane.QUESTION_MESSAGE));
                	vet_mc[i] = e;
                	}
                	int ini_mc = 0;
                	int fim_mc = tam_mc-1;
                	Ordena.mergeSort( 0, vet_mc, ini_mc, fim_mc );               	
//                	System.out.println("\nMerge");
                	for ( int i=0; i< vet_mc.length; i++ ){
//                		System.out.print(" " + vet[i]);
                		msg = msg + " " + vet_mc[i];
                	}
                	JOptionPane.showMessageDialog(null, "Merge: " + msg, "Merge Sort resultado…", JOptionPane.INFORMATION_MESSAGE);
                	break;
                	
                case "7": //Ordena Decrescente Merge Sort
                	int tam_md = Integer.parseInt(JOptionPane.showInputDialog(null, "Digite o tamanho do Vetor: ",
                			"Merge Sort", JOptionPane.QUESTION_MESSAGE));
                	int vet_md[] = new int[tam_md];
                	for (  int i = 0;  i < vet_md.length; i++ ){
                	int e = Integer.parseInt(JOptionPane.showInputDialog(null, "Digite o elemento do Índice [" + i + "]: ",
                			"Elementos do Vetor", JOptionPane.QUESTION_MESSAGE));
                	vet_md[i] = e;
                	}
                	int ini_md = 0;
                	int fim_md = tam_md-1;
                	Ordena.mergeSort( 1, vet_md, ini_md, fim_md );               	
//                	System.out.println("\nMerge");
                	for ( int i=0; i< vet_md.length; i++ ){
//                		System.out.print(" " + vet[i]);
                		msg = msg + " " + vet_md[i];
                	}
                	JOptionPane.showMessageDialog(null, "Merge: " + msg, "Merge Sort resultado…", JOptionPane.INFORMATION_MESSAGE);
                	break;
                	
                case "8": //Ordena Crescente Quik Sort
                	int tam_qc = Integer.parseInt(JOptionPane.showInputDialog( null, "Digite o tamanho do Vetor: ",
                			"Quick Sort", JOptionPane.QUESTION_MESSAGE ));
                	int vet_qc[] = new int[tam_qc];
                	for (  int i = 0;  i < vet_qc.length; i++ ){
                	int e = Integer.parseInt( JOptionPane.showInputDialog(null, "Digite o elemento do Índice [" + i + "]: ",
                			"Elementos do Vetor", JOptionPane.QUESTION_MESSAGE ));
                	vet_qc[i] = e;
                	}
                	int ini_qc = 0;
                	int fim_qc = tam_qc-1;
                	Ordena.quick( 0, vet_qc, ini_qc, fim_qc );
//                	System.out.println("\nQuick");
                	for ( int i = 0; i < vet_qc.length; i++ ){
//                		System.out.print(" " + vet[i]);
                		msg = msg + " " + vet_qc[i];
                	}
                	JOptionPane.showMessageDialog( null, "Quick: " + msg, "Quick Sort resultado…", JOptionPane.INFORMATION_MESSAGE);
                    break;
                  
                case "9": //Ordena Decrescente Quik Sort
                	msg = "";
                	int tam_qd = Integer.parseInt( JOptionPane.showInputDialog(null, "Digite o tamanho do Vetor: ",
                			"Quick Sort", JOptionPane.QUESTION_MESSAGE ));
                	int vet_qd[] = new int[tam_qd];
                	for (  int i = 0;  i < vet_qd.length; i++ ){
                	int e = Integer.parseInt(JOptionPane.showInputDialog( null, "Digite o elemento do Índice [" + i + "]: ",
                			"Elementos do Vetor", JOptionPane.QUESTION_MESSAGE ));
                	vet_qd[i] = e;
                	}
                	int ini_qd = 0;
                	int fim_qd = tam_qd-1;
                	Ordena.quick( 1, vet_qd, ini_qd, fim_qd );              	
//                	System.out.println("\nQuick");
                	for ( int i=0; i < vet_qd.length; i++ ){
//                		System.out.print( " " + vet[i] );
                		msg = msg + " " + vet_qd[i];
                	}
                	JOptionPane.showMessageDialog( null, "Quick: " + msg, "Quick Sort resultado…", JOptionPane.INFORMATION_MESSAGE);
                    break;
                    
                case "0":
                    return;
                default://Tratamento de Erro
                    JOptionPane.showMessageDialog(null, "Por favor, escolha as Opções do Menu!");
                    break;
            }
        } while (opt != "0");
    }
}