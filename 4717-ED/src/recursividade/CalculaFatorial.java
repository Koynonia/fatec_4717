package recursividade;

public class CalculaFatorial {

	public static void main(String[] args) {
		
	
	}
	
//	FATORIAL SEM RECURSIVIDADE
	
	public int fatorial ( int n ){
		
		int f = 1;
		
		for ( int i = n; i > 1; i--){
			f = f * i;
		}
		System.out.println( f );
		return f;
	}


//  FATORIAL COM RECURSIVIDADE ( com Cristéio de Continuidade )

	public int fatC( int n ){

		if ( n > 1 )
			return n * fatC ( n - 1 );
		
		System.out.println( n );
		return 1;
	}


//  FATORIAL COM RECURSIVIDADE ( com Cristéio de Parada )

	public int fatP( int n ){

		if ( n == 0 || n ==1 )
			return 1;
		
		System.out.println( n );
		return n * fatP ( n - 1 );
	}
}