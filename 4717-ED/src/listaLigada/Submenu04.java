/*
 * 15/02/2016 Revisão Aritmética
 * @author 142016-1-Fernando Moraes Oliveira
 */
package listaLigada;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Submenu04 {

    public static void main(String args[]) {

        ListaLigada ll1 = new ListaLigada();

        String opt;
        int n;
        int p;

        do {
        	opt = (String) JOptionPane.showInputDialog( null, 
                    "Escolha uma Opção:\n\n"
                    + "01 - Adiciona Início\n"
                    + "02 - Adiciona Final\n"
                    + "03 - Adiciona por Posição (v.1)\n"
                    + "04 - Adiciona por Posição (v.2)\n"
                    + "05 - Remove Início\n"
                    + "06 - Remove Final\n"
                    + "07 - Remove por Posição\n"
                    + "08 - Percorre\n"
                    + "00 - FIM\n\n"
                    ,"Menu Lista Ligada", JOptionPane.QUESTION_MESSAGE, 
                    new ImageIcon("../4717-ED/icones/bot.png"), null, null);
            switch (opt) {
                case "1": //ADICIONA INICIO
                    n = Integer.parseInt(JOptionPane.showInputDialog("Digite um Número Inteiro: "));
                    ll1.adicionaInicio(n);
                    ll1.percorre();
                    break;
                case "2": //ADICIONA FINAL
                    n = Integer.parseInt(JOptionPane.showInputDialog("Digite um Número Inteiro: "));
                    ll1.adicionaFinal(n);
                    ll1.percorre();
                    break;
                case "3"://ADICIONA POSIÇÃO V1
                    n = Integer.parseInt(JOptionPane.showInputDialog("Digite um Número Inteiro: "));
                    p = Integer.parseInt(JOptionPane.showInputDialog("Digite uma Posição: "));
                    ll1.adicionaPosicao(n, p);
                    ll1.percorre();
                    break;
                case "4"://ADICIONA POSIÇÃO V2
                    n = Integer.parseInt(JOptionPane.showInputDialog("Digite um Número Inteiro: "));
                    p = Integer.parseInt(JOptionPane.showInputDialog("Digite uma Posição: "));
                    ll1.adicionaPosicao2(n, p);
                    ll1.percorre();
                    break;
                case "5": //REMOVE INICIO
                    ll1.removeInicio();
                    ll1.percorre();
                    break;
                case "6": //REMOVE FINAL
                    ll1.removeFinal();
                    ll1.percorre();
                    break;
                case "7": //REMOVE POSIÇÃO
                    p = Integer.parseInt(JOptionPane.showInputDialog("Digite uma Posição: "));
                    ll1.removePosicao(p);
                    ll1.percorre();
                    break;
                case "8": //PERCORRE
                    ll1.percorre();
                    break;
                case "0":
                    return;
                default://Tratamento de Erro
                    JOptionPane.showMessageDialog(null, "Por favor, escolha as Opções do Menu!");
                    break;
            }
        } while (opt != "0");
    }
}
