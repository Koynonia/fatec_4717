/*
 * 09/05/2-16
 * @author 142016-1 Fernando Moraes Oliveira
 */
package tabelaEspalhamento;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

public class Submenu05 {

    public static void main(String args[]) {


        int opt;
        String nf;
        String end;
        String nome;
        
        TabelaEspEntrega t = new TabelaEspEntrega();

        do {
            opt = Integer.parseInt(JOptionPane.showInputDialog(
                    "Escolha uma Opção:\n\n"
                    + "01 - Adiciona ENTREGA\n"
                    + "02 - Percorre para FRENTE\n"
                    + "00 - FIM\n\n"));
            
            switch (opt) {
                case 1: //ADICIONA ENTREGA
                    nf = JOptionPane.showInputDialog("Digite a Nota Fiscal: ");
                    if ( ! testarNumero( nf ) ){
                    	msg( "erroDigit", "Nota Fiscal: " + nf );
                    	Submenu05.main(args);
                    	return;
                    }
                    end = JOptionPane.showInputDialog("Digite o Endereço da Entrega: ");
                    nome = JOptionPane.showInputDialog("Digite o Nome da Entrega: ");
                    t.adiciona(new Entrega( Integer.parseInt( nf ),end,nome),10);
                    JOptionPane.showMessageDialog(null, "Entrega: \n\n" 
                    + "Nota Fiscal: " + nf + "\nEndereço: " + end + "\nNome: " + nome + "\n\nfoi adicionada com sucesso!");
                    break;
                case 2: //PERCORRE FRENTE
                	t.percorreFrente();
                    break;
                case 0:
                    break;
                default://Tratamento de Erro
                    JOptionPane.showMessageDialog(null, "Por favor, escolha as Opções do Menu!");
                    break;
            }
        } while (opt != 0);
    }
    
    
    public static boolean testarNumero(String str) {
		Pattern p = Pattern.compile("[0-9]+");
		Matcher m = p.matcher(str);
		return m.find();    
	}
    
    
    public static void msg( String tipo, String mensagem ) {

    	switch ( tipo ) {

    	case "erroDigit":
    		JOptionPane.showMessageDialog(null, 
    				"Entrada inválida:\n\n" +
    						mensagem +
    						"\n\nPor favor, entre somente com números para a quantidade.", 
    						"Entrada Inválida…", 
    						JOptionPane.QUESTION_MESSAGE);
    		break;

    	default:
    		JOptionPane.showMessageDialog(null, 
    				mensagem, 
    				"Erro no Sistema", 
    				JOptionPane.QUESTION_MESSAGE);
    	}
    }
}
