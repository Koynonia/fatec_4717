/**
Classe de um no com elemento entrega
*/

package tabelaEspalhamento;

public class NoEntrega {
    /** dados da entrega*/
    public Entrega dado;
	 /** referencia do próximo no*/
    public NoEntrega prox;
	 /** referencia do no anterior*/
    public NoEntrega ant;

    /** 
	 Construtor
	 @param dados da entrega
	 */
    public NoEntrega(Entrega e){
        dado = e;
        prox = null;
        ant = null;
    }
}
