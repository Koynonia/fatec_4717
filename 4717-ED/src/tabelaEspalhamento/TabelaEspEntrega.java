/**
Tabela de Espalhamento que organiza entrega por zonas: leste, oe
*/

package tabelaEspalhamento;
import javax.swing.JOptionPane;

public class TabelaEspEntrega {
	 /**Pilha com as entregas por região*/
    private ListaLLEntrega pilha[];

	 /**
	 Construtor
	 */
    public TabelaEspEntrega(){
        pilha = new ListaLLEntrega[5];
    }

	 /**
	 Método que calcula o índice do novo elemento de acordo com a zona de entrega
	 @param codigo código da zona de entrega
	 */
    public int funcaoEspalhamento(int codigo){
        int v = codigo - 10;
        return v;
    }

	 /**
	 Método que adiciona nova entrega à tabela de espalhamento
	 */
    public void adiciona(Entrega novo, int codigo){
        if(codigo >= 10 && codigo <= 14){
            int indice = funcaoEspalhamento(codigo);
            if(pilha[indice] == null)
                pilha[indice] = new ListaLLEntrega();
            pilha[indice].empilha(novo);
        } else
            JOptionPane.showMessageDialog(null, "código Inválido");
    }

	 /**
	 Método que remove uma entrega da tabela de espalhamento
	 @param codigo código da zona aonde o produto foi entregue
	 */
    public Entrega remove(int codigo){
        if(codigo >= 10 && codigo <= 14){
            int indice = funcaoEspalhamento(codigo);
            if(pilha[indice] == null){
                JOptionPane.showMessageDialog
                        (null, "Não há Pilha para Esta região!");
                return null;
            }
                return pilha[indice].desempilha();
        } else {
            JOptionPane.showMessageDialog(null, "código Inválido!");
            return null;
        }
    }
	 /**
	 Método que percorre a tabela de espalhamento, concatenando o conteúdo em uma String.
	 Utiliza o Método percorreFrente da Lista Ligada.
	 @return conteúdo da tabela de espalhamento
	 */
    public String percorreFrente(){
        String r = "";
        for(int i = 0; i < pilha.length; i++)
            if(pilha[i] != null)
                r += pilha[i].percorreFrente() + "\n";
        return r;
    }
	 /**
	 Método que percorre a tabela de espalhamento, concatenando o conteúdo em uma String.
	 Utiliza o Método da lista ligada percorreTras()
	 @return conteúdo da tabela de espalhamento
	 */
    public String percorreTras(){
        String r = "";
        for(int i = 0; i < pilha.length; i++)
            if(pilha[i] != null)
                r += pilha[i].percorreTras() + "\n";
        return r;
    }
	 
	 //Principal
	 
	 public static void main(String args[]){
		TabelaEspEntrega t = new TabelaEspEntrega();
		System.out.println("Adicionando elementos....");
		t.adiciona(new Entrega(1,"AAA","AAA"),10);
		t.adiciona(new Entrega(2,"BBB","BBB"),13);
		t.adiciona(new Entrega(3,"CCC","CCC"),13);
		t.adiciona(new Entrega(4,"DDD","DDD"),10);			 
		t.adiciona(new Entrega(5,"EEE","EEE"),13);
		t.adiciona(new Entrega(6,"FFF","FFF"),13);
		System.out.println("Percorre para a frente");
		t.percorreFrente();
		System.out.println("Percorre para tr�s");
		t.percorreTras();
		System.out.println("Removendo.....");
		Entrega n=t.remove(10);
		System.out.println("Removido "+n.toString());
		n=t.remove(11);
		n=t.remove(13);
		System.out.println("Removido "+n.toString());
		System.out.println("Percorre para a frente");
		t.percorreFrente();
		System.out.println("Percorre para tr�s");
		t.percorreTras();

	 }
}
