/**
Classe que controla uma lista de entrega como uma pilha
*/

package tabelaEspalhamento;
import javax.swing.JOptionPane;

public class ListaLLEntrega {
	 /** referência ao topo*/
    private NoEntrega topo;
	 /** referência à base*/
    private NoEntrega base;

	 /**
	 Construtor
	 */
    public ListaLLEntrega(){
        topo = null;
        base = null;
    }

	 /**
	 Método que testa se a lista está vazia
	 @return true se a lista está vazia e false se houver algum elemento
	 */
    public boolean vazia(){
        return topo == null;
    }

	 /**
	 Método que adiciona a nova entrega ao topo
	 @param e nova entrega
	 */
    public void empilha(Entrega e){
        NoEntrega novo = new NoEntrega(e);
        if(vazia()){
            topo = novo;
            base = novo;
        } else {
		  		topo.ant = novo;
            novo.prox = topo;
            topo = novo;
            topo.ant = null;
        }
    }

	 /**
	 Método que desempilha o elemento do topo e o retorna
	 @return retorna a entrega que foi removida
	 */
    public Entrega desempilha(){
        Entrega des = null;
        if(vazia())
            JOptionPane.showMessageDialog(null, "Pilha Vazia!");
        else{
            des = topo.dado;
            topo = topo.prox;
            topo.ant = null;
        }
        return des;
    }

	 /**
	 Método que percorre a lista do topo à base
	 @return conteúdo da lista
	 */
    public String percorreFrente(){
        NoEntrega n = topo;
        String r = "";
        while(n != null){
            System.out.println(" " + n.dado.toString());
            r = r + n.toString() + "\n";
            n = n.prox;
        }
        System.out.println(" ");
        return r;
    }
	 
	 /**
	 Método que percorre a lista da base para o topo
	 @return conteúdo da lista
	 */
	 public String percorreTras(){
        NoEntrega n = base;
        String r = "";
        while(n != null){
            System.out.println(" " + n.dado.toString());
            r = r + n.toString() + "\n";
            n = n.ant;
        }
        System.out.println(" ");
        return r;
    }
}
