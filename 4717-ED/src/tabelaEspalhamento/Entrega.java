/**
Classe para controle de entregas por número de nota fiscal, endereço e nome do 
destinatário.
*/

package tabelaEspalhamento;

public class Entrega {
	 /**número da nota fiscal*/
    private int notaFiscal;
	 /**endereço completo*/
    private String endereco;
	 /**nome do destinatário*/
    private String nome;

	 /**
	 Construtor
	 @param notaFiscal número da nota fiscal
	 @param endereco endereço completo de entrega
	 @param nome nome completo do destinatário
	 */
    public Entrega(int notaFiscal, String endereco, String nome) {
        this.notaFiscal = notaFiscal;
        this.endereco = endereco;
        this.nome = nome;
    }

	 /**
	 Método de acesso ao endereço completo
	 @return endereço completo
	 */
    public String getEndereco() {
        return endereco;
    }

	 /**
	 Método modificador do endereço
	 @param endereco novo endereço
	 */
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

	 /**
	 Método de acesso ao nome completo
	 @return nome completo
	 */
    public String getNome() {
        return nome;
    }

	 /**
	 Método modificador do nome do destinatário
	 @param nome novo nome de destinatário
	 */
    public void setNome(String nome) {
        this.nome = nome;
    }

	 /**
	 Método de acesso ao número da nota fiscal
	 @return número da nota fiscal
	 */
    public int getNotaFiscal() {
        return notaFiscal;
    }

	 /**
	 Método modificador do número da nota fiscal
	 @param notaFiscal novo número de nota fiscal
	 */
    public void setNotaFiscal(int notaFiscal) {
        this.notaFiscal = notaFiscal;
    }

	 /**
	 Método que retorna os valores dos atributos de uma instância de entrega
	 @return String com os valores dos atributos
	 */
    @Override
    public String toString() {
        return "Entrega{" + "notaFiscal=" + notaFiscal + "endereco=" + endereco + "nome=" + nome + '}';
    }
}