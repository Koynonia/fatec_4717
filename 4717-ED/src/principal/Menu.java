/*
 * 15/02/2016 Menu Principal
 * @author 142016-1-Fernando Moraes Oliveira
 */
package principal;

import java.util.Random;

import intro.Submenu01;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import arvoreBinaria.Submenu07;
import pilhasFilas.Submenu03;
import recursividade.Submenu06;
import tabelaEspalhamento.Submenu05;
import listaLigada.Submenu04;
import listas.Submenu02;

public class Menu {

    public static void main(String args[]) {

        String opt;

        do {
            opt = (String) JOptionPane.showInputDialog( null, 
                    "Escolha uma Opção:\n\n"
                    + "01 - Introdução\n"
                    + "02 - Listas\n"
                    + "03 - Filas e Pilhas\n"
                    + "04 - Lista Ligada\n"
                    + "05 - Tebela de Espalhamento\n"
                    + "06 - Recursividade\n"
                    + "07 - Árvore Binária\n"
                    + "00 - FIM\n\n"
                    ,"Menu Principal", JOptionPane.QUESTION_MESSAGE, 
                    new ImageIcon("../4717-ED/icones/" + escolherIcone() + ".png"), null, null);
            
            switch (opt) {
                case "1":// INTRO
                    Submenu01.main(args);
                    break;
                case "2":// LISTA
                    Submenu02.main(args);
                    break;
                 case "3":// PILHAS E FILHAS
                    Submenu03.main(args);
                    break;
                 case "4"://LISTA LIGADA
                     Submenu04.main(args);
                     break;
                 case "5"://TEBELA DE ESPALHAMENTO
                     Submenu05.main(args);
                     break;
                 case "6"://RECURSIVIDADE
                     Submenu06.main(args);
                     break;
                 case "7"://RECURSIVIDADE
                     Submenu07.main(args);
                     break;
                case "0":
                    return;
                default://Tratamento de Erro
                    JOptionPane.showMessageDialog(null, "Por favor, escolha as Opções do Menu!");
                    break;
            }
        } while (opt != "0");
    }
    
    public static int escolherIcone(){
    	Random gerador = new Random();
    	return gerador.nextInt(10);
    }  

}